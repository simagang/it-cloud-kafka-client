package com.it.cloud.producer.dto;

import java.io.Serializable;

/**
 * 消息实体
 */
public class MessageInfo implements Serializable {

    private static final long serialVersionUID = 1L;

    private String id;

    private String msg;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "MessageInfo{" +
                "id='" + id + '\'' +
                ", msg='" + msg + '\'' +
                '}';
    }
}
