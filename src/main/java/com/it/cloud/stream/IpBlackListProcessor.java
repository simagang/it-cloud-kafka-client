package com.it.cloud.stream;

import org.apache.kafka.streams.kstream.Windowed;
import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;

/**
 * 自定义Processor完成黑名单处理
 */
public class IpBlackListProcessor implements Processor<Windowed<String>, Long> {

    @Override
    public void init(ProcessorContext context) {

    }

    @Override
    public void process(Windowed<String> key, Long value) {
        System.out.println("ip:" + key.key() + "被加入到黑名单, 请求次数为:" + value);
    }

    @Override
    public void punctuate(long timestamp) {

    }

    @Override
    public void close() {

    }
}
